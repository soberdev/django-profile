# django profile

## install requirements
### use python env and activate
```
pip install -r requirements.txt
```

## running the app for firstime
### create migration and database
#### this app use sqlite database, you dont need to setup anything
- make migration file
```
python manage.py makemigration
```
- do migrate/change to database
```
python manage.py migrate
```
- creating a user <br> this app should have one user, see my_profiles/views.py line 13 , or you can edit with your user ID if you have other
```
python manage.py createsuperuser
```
fill username, email and your password
### editing the profile
navigate to [http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin)  login with superuser that you've been created in previous step, then create your profiles.
- you can search the icon for your needs in [iconify](https://icon-sets.iconify.design/) and simply copy the span element like example below, paste it in icon fields
```
<span class="iconify" data-icon="material-symbols:10k-outline"></span>
```

### use this command for production
- collect staticfiles
```
python manage.py collectstatic --noinput
```
- run the server under port 8080
```
gunicorn --bind 0.0.0.0:8080 core.wsgi:application
```

