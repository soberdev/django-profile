from django.contrib import admin
from .models import Profile, Skills, Social, Experiences


class SkillsAdmin(admin.ModelAdmin):
    list_editable = ["_icon"]
    list_display = ["name", "skill_level", "skill_type", "_icon"]
    list_filter = ["skill_type"]


admin.site.register(Skills, SkillsAdmin)
admin.site.register(Profile)
admin.site.register(Social)
admin.site.register(Experiences)
