from django.db import models
from django.contrib.auth import get_user_model
from django.core.files.storage import FileSystemStorage

fs = FileSystemStorage(location="media")


class Profile(models.Model):
    user = models.OneToOneField(
        get_user_model(), on_delete=models.CASCADE, related_name="profile"
    )
    fullname = models.CharField(max_length=255, blank=True)
    job_stack = models.CharField(max_length=255, blank=True)
    email = models.EmailField()
    phone = models.CharField(max_length=20, null=True, blank=True)
    image = models.ImageField(storage=fs, upload_to="images/", blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=255, blank=True)
    country = models.CharField(max_length=255, blank=True)
    about = models.TextField(blank=True)

    def __str__(self):
        return self.user.username

    @property
    def get_full_address(self):
        return f"{self.city}, {self.state}, {self.country}"


class Skills(models.Model):
    SKILL_TYPE = (
        ("Language", "Language"),
        ("Framework", "Framework"),
        ("Tool", "Tool"),
        ("Database", "Database"),
        ("Other", "Other"),
    )
    profile = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name="profile_skills"
    )
    skill_type = models.CharField(max_length=255, choices=SKILL_TYPE)
    name = models.CharField(max_length=255)
    skill_level = models.IntegerField(default=1)
    _icon = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name

    @property
    def icon(self):
        if self._icon:
            return self._icon
        return ""


class Social(models.Model):
    user = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="profile_social"
    )
    name = models.CharField(max_length=255)
    url = models.URLField()
    _icon = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name

    @property
    def icon(self):
        if self._icon:
            return self._icon
        return ""


class Experiences(models.Model):
    user = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="profile_experiences"
    )
    title = models.CharField(max_length=255)
    company = models.CharField(max_length=255)
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    description = models.TextField()
    image = models.ImageField(storage=fs, upload_to="images/", blank=True, null=True)
    url = models.URLField(blank=True, null=True)

    @property
    def until(self):
        if self.end_date:
            return self.end_date
        return "Present"

    def __str__(self):
        return self.company
