from django.views.generic import TemplateView
from django.contrib.auth.models import User
from django.shortcuts import render
from django.db.utils import OperationalError


class ProfileViews(TemplateView):
    template_name: str = "index.html"

    def get_context_data(self, **kwargs: any) -> dict[str, any]:
        contexts = super().get_context_data(**kwargs)
        try:
            user = User.objects.get(id=1)
        except User.DoesNotExist:
            return contexts
        except OperationalError:
            return contexts
        contexts["User"] = user
        contexts["Language"] = user.profile.profile_skills.filter(
            skill_type="Language"
        ).order_by("-skill_level")
        contexts["Framework"] = user.profile.profile_skills.filter(
            skill_type="Framework"
        ).order_by("-skill_level")
        contexts["Tool"] = user.profile.profile_skills.filter(
            skill_type="Tool"
        ).order_by("-skill_level")
        contexts["Database"] = user.profile.profile_skills.filter(
            skill_type="Database"
        ).order_by("-skill_level")
        contexts["Other"] = user.profile.profile_skills.filter(
            skill_type="Other"
        ).order_by("-skill_level")
        return contexts


def handler404(request, exception):
    context = {}
    response = render(request, "error_page.html", context=context)
    response.status_code = 404
    return response


def handler500(request):
    context = {}
    response = render(request, "error_page.html", context=context)
    response.status_code = 500
    return response
